#ifndef IDC_STATIC
#define IDC_STATIC (-1)
#endif

#define DLG_MAIN                                100
#define IDI_ICON                                103
#define IDC_STC_LOG                             1000
#define IDC_COMBO_PROCESSLIST                   1001
#define IDC_EDT_ADDR                            1002
#define IDC_BTN_ANALYZE                         1003
#define IDC_EDT_SIZE                            1004
#define IDC_EDT_NUMSPLICES                      1006
#define IDC_CHK_ONTOP                           1007
#define IDC_BTN_REBASE                          1009
#define IDC_EDT_BASE                            1010
#define IDC_BTN_REMOVE                          1011
#define IDC_LIST_SPLICES                        1013
