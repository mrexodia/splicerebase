#include <windows.h>
#include <stdio.h>
#include "BeaEngine/BeaEngine.h"

#pragma pack(push,4)

/**
Public Type REGTYPE
    REG_Kind      As Byte  ' ;1=8 bits \ 2=16 bits \ 3=32 bits \ 4=MMX \ 5=XMM \ 6=Float stack \ 7=Segment \ 8=Debug \ 9=Control \ 10=Test
    REG_Ptr_Kind  As Byte  ' ;1=Byte PTR \ 2=Word PTR \ 3=Dword PTR \ 4=Qword PTR \ 5=mmword ptr \ 6=xmmword ptr \ 7=FWord PTR \ 8=tbyte ptr \ 9=null ptr (LEA)
    REG_Type      As Byte  ' ;0-7= direct register index \ 16 register=byte && 7 \ 32 register=(byte && 63)/8 \ 64=[32/16 address only] \ 128=[using x86 relatives]
    REG_BaseAsReg As Byte  ' ;1=Register only (BASE exposed)!
End Type

Public Type REGSTRUCT
    SEG_TYPE            As Long
    Base                As Long
    Index               As Long
    SCALE               As Long
    DISPLACEMENTS       As Long
    DISPLACEMENT_TYPE   As Long
    REG_Kind            As REGTYPE
    PTR_TYPE            As Long
End Type

Public Type IMMSTRUCT
    VALUE_LO            As Long
    VALUE_HI            As Long
    VALUE_TYPE          As Long     ' 1=Byte \ 2=Word \ 4=Dword \ 8=ByteToWord \ 16=ByteToDword \ 32=AbsJump \ 64=ShortJump \ 128=LongJump
End Type

Public Type DisAsmStruct
    Instruction_Prefix  As Long
    Instruction         As Long
    Reg1                As REGSTRUCT
    Reg2                As REGSTRUCT
    Reg_Reg             As Long     ' 1=from ptr
    Imm                 As IMMSTRUCT
    Instruction_Length  As Long
End Type
*/

struct REGTYPE
{
    BYTE REG_Kind; //1=8 bits \ 2=16 bits \ 3=32 bits \ 4=MMX \ 5=XMM \ 6=Float stack \ 7=Segment \ 8=Debug \ 9=Control \ 10=Test
    BYTE REG_Ptr_Kind; //1=Byte PTR \ 2=Word PTR \ 3=Dword PTR \ 4=Qword PTR \ 5=mmword ptr \ 6=xmmword ptr \ 7=FWord PTR \ 8=tbyte ptr \ 9=null ptr (LEA)
    BYTE REG_Type; //0-7= direct register index \ 16 register=byte && 7 \ 32 register=(byte && 63)/8 \ 64=[32/16 address only] \ 128=[using x86 relatives]
    BYTE REG_BaseAsReg; //1=Register only (BASE exposed)!
};

struct REGSTRUCT
{
    long SEG_TYPE;
    long Base;
    long Index;
    long SCALE;
    long DISPLACEMENTS;
    long DISPLACEMENT_TYPE;
    REGTYPE REG_Kind;
    long PTR_TYPE;
};

struct IMMSTRUCT
{
    long VALUE_LO;
    long VALUE_HI;
    long VALUE_TYPE; //1=Byte \ 2=Word \ 4=Dword \ 8=ByteToWord \ 16=ByteToDword \ 32=AbsJump \ 64=ShortJump \ 128=LongJump
};

struct DisAsmStruct
{
    long Instruction_Prefix;
    long Instruction;
    REGSTRUCT Reg1;
    REGSTRUCT Reg2;
    long Reg_Reg; //1=from ptr
    IMMSTRUCT Imm;
    long Instruction_Length;
};

#pragma pack(pop)

///Public Declare Function Disassemble Lib "Disasm.dll" Alias "DisAssemble" (DATA As Any, ByVal BaseAddress As Long, DisAsmString As Any, DisAsmS As Any, ByVal DisasmOpt As Long) As Long
extern "C" __declspec(dllexport) __stdcall long DisAssemble(unsigned char* DATA, const long BaseAddress, char* DisAsmString, DisAsmStruct* DisAsmS, const long DisasmOpt)
{
    if(!DATA || !DisAsmString || !DisAsmS)
        return 0;
    // Reset Disasm Structure
    DISASM disasm;
    memset(&disasm, 0, sizeof(DISASM));
    disasm.Options=NoformatNumeral;
    disasm.VirtualAddr=BaseAddress;
    disasm.EIP=(UIntPtr)DATA;
    int len=Disasm(&disasm);
    char instruction[INSTRUCT_LENGTH]="";
    if(len==UNKNOWN_OPCODE)
    {
        len=1;
        strcpy(instruction, "???");
    }
    else
        strcpy(instruction, disasm.CompleteInstr);
    _strupr(instruction); //uppercase
    int instrlen=strlen(instruction);
    for(int i=0,j=0; i<instrlen; i++) //remove spaces between arguments
    {
        if(instruction[i]==',' && instruction[i+1]==' ')
        {
            j+=sprintf(DisAsmString+j, ",");
            i++;
        }
        else
            j+=sprintf(DisAsmString+j, "%c", instruction[i]);
    }
    memset(DisAsmS, 0, sizeof(DisAsmStruct));
    DisAsmS->Instruction_Length=len;
    return strlen(DisAsmString); //return text length
}

extern "C" __declspec(dllexport) BOOL APIENTRY DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
    /*if(fdwReason==DLL_PROCESS_ATTACH)
    {
        char test[256]="";
        char instr[256]="";
        DisAsmStruct disasm;
        unsigned char* DATA=(unsigned char*)hinstDLL+0x1000;
        do
        {
            int len=DisAssemble(DATA, (long)DATA, instr, &disasm, 0);
            sprintf(test, "len:%d\ninstr:\"%s\"\ninstrlen:%d", len, instr, (int)disasm.Instruction_Length);
            DATA+=disasm.Instruction_Length;
        }
        while(MessageBoxA(0, test, "test", MB_YESNO|MB_SYSTEMMODAL)==IDYES);
    }*/
    return TRUE;
}
