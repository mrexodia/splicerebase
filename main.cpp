#include <windows.h>
#include <tlhelp32.h>
#include <commctrl.h>
#include <stdio.h>
#include <ctype.h>
#include "resource.h"
#include "BeaEngine\BeaEngine.h"

//Structure definitions
struct PROCESS_LIST
{
    unsigned int pid; //process id
    char process_name[512]; //name of the process
};

struct SPLICE_ENTRY
{
    unsigned int va; //virtual address of a block (start address)
    unsigned int size; //size of a block
    unsigned char* data; //pointer to the start of a block (in memory)
    unsigned int jmp_va; //virtual address of the jump leading to this block
};

struct SPLICE_INFO
{
    unsigned int va; //virtual address of the code splicing page
    unsigned int size; //size of the code splicing page
    unsigned char* data; //pointer to a copy of the page
    SPLICE_ENTRY* splices; //pointer to all blocks
    unsigned int total_splices; //total number of blocks
};

//Global vars
HINSTANCE hInst;
int process_count=0;
unsigned int current_pid=0;
SPLICE_INFO* splice_info;
PROCESS_LIST* processlist;

//Global functions
bool rpm(HANDLE hProcess, long addr, void* buffer, unsigned int size) //super-safe implementation
{
    unsigned char* newBuffer=(unsigned char*)buffer;
    for(unsigned int i=0; i<size; i++)
    {
        DWORD oldprotect=0;
        VirtualProtectEx(hProcess, (void*)(newBuffer+i), 1, PAGE_EXECUTE_READWRITE, &oldprotect);
        bool ret=ReadProcessMemory(hProcess, (const void*)(addr+i), newBuffer+i, 1, 0);
        VirtualProtectEx(hProcess, (void*)(newBuffer+i), 1, oldprotect, &oldprotect);
        if(!ret)
           return false;
    }

    return true;
}

bool wpm(HANDLE hProcess, long addr, void* buffer, unsigned int size) //super-safe implementation
{
    unsigned char* newBuffer=(unsigned char*)buffer;
    for(unsigned int i=0; i<size; i++)
    {
        DWORD oldprotect=0;
        VirtualProtectEx(hProcess, (void*)(newBuffer+i), 1, PAGE_EXECUTE_READWRITE, &oldprotect);
        bool ret=WriteProcessMemory(hProcess, (void*)(addr+i), newBuffer+i, 1, 0);
        VirtualProtectEx(hProcess, (void*)(newBuffer+i), 1, oldprotect, &oldprotect);
        if(!ret)
            return false;
    }
    return true;
}

bool FindPattern1(BYTE* d, unsigned int size)
{
    for(unsigned int i=0; i<size; i++) //7???7?
        if((d[i]>>4)==0x07 and(d[i+2]>>4)==0x07)
            return true;
    return false;
}

bool FindPattern2(BYTE* d, unsigned int size)
{
    for(unsigned int i=0; i<size; i++) //8BFF
        if(d[i]==0x8B and d[i+1]==0xFF)
            return true;
    return false;
}

bool FindPattern3(BYTE* d, unsigned int size)
{
    for(unsigned int i=0; i<size; i++) //F7D?
        if(d[i]==0xF7 and(d[i+1]>>4)==0x0D)
            return true;
    return false;
}

bool FindPattern4(BYTE* d, unsigned int size)
{
    for(unsigned int i=0; i<size; i++) //9090909090
        if(d[i]==0x90 and d[i+1]==0x90 and d[i+2]==0x90 and d[i+3]==0x90 and d[i+4]==0x90)
            return true;
    return false;
}

void PrintError(HWND hwndDlg, const char* text)
{
    SetDlgItemTextA(hwndDlg, IDC_STC_LOG, text);
    MessageBeep(MB_ICONERROR);
}

//Get a list of all processes (Thx to Aguila)
bool GetProcessList(HWND combo)
{
    if(processlist)
        free(processlist);
    process_count=0;
    processlist=(PROCESS_LIST*)malloc(sizeof(PROCESS_LIST)*1024);
    memset(processlist, 0, sizeof(PROCESS_LIST)*1024);
    HANDLE hProcessSnap;
    PROCESSENTRY32 pe32;
    hProcessSnap=CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
    if(hProcessSnap==INVALID_HANDLE_VALUE)
        return false;
    pe32.dwSize=sizeof(PROCESSENTRY32);
    if(!Process32First(hProcessSnap, &pe32))
    {
        CloseHandle(hProcessSnap);
        return false;
    }
    do
    {
        if(pe32.th32ProcessID and strcasecmp(pe32.szExeFile, "system"))
        {
            processlist[process_count].pid=pe32.th32ProcessID;
            strcpy(processlist[process_count].process_name, pe32.szExeFile);
            process_count++;
        }
    }
    while(Process32Next(hProcessSnap, &pe32));
    CloseHandle(hProcessSnap);
    SendMessageA(combo, CB_RESETCONTENT, 0, 0);
    for(int i=process_count-1; i>-1; i--)
    {
        char str[512]="";
        sprintf(str, "PID: %.4X, Name: %s\n", processlist[i].pid, processlist[i].process_name);
        SendMessageA(combo, CB_ADDSTRING, 0, (LPARAM)str);
    }
    SendMessageA(combo, CB_SETCURSEL, 0, 0);
    SendMessageA(GetParent(combo), WM_COMMAND, MAKEWPARAM(GetDlgCtrlID(combo), CBN_SELCHANGE), 0);
    return true;
}

//Checks whether a page contains at least 2 markers
bool IsCodeSplicingPage(HANDLE hProcess, unsigned int base)
{
    unsigned char* bytes=(unsigned char*)malloc(0x100);
    if(!rpm(hProcess, base, bytes, 0x100))
    {
        free(bytes);
        return false;
    }
    int found_count=0;
    if(FindPattern1(bytes, 0x100))
        found_count++;
    if(FindPattern2(bytes, 0x100))
        found_count++;
    if(FindPattern3(bytes, 0x100))
        found_count++;
    if(found_count>1)
        return true;
    else if(FindPattern4(bytes, 0x100))
        return true;
    return false;
}

//Check the full memory range of the select EXE for potentional pages (Thx to Admiral)
bool CheckSplices(HWND hwndDlg)
{
    HANDLE hProcess=OpenProcess(PROCESS_ALL_ACCESS, 0, current_pid);
    unsigned int aa=GetTickCount();
    if(!hProcess)
        return false;
    unsigned int i=0x10000;
    int j=0;
    unsigned int potentional[100]= {0};
    MEMORY_BASIC_INFORMATION mbi= {0};
    while(i<0xFFF0000)
    {
        if(VirtualQueryEx(hProcess, (void*)i, &mbi, sizeof(MEMORY_BASIC_INFORMATION)))
        {
            if(mbi.State!=MEM_FREE and mbi.State!=MEM_RESERVE and mbi.Type==MEM_PRIVATE and mbi.RegionSize>=0x10000 and mbi.RegionSize<=0x20000 and mbi.Protect>=PAGE_EXECUTE and mbi.Protect<=PAGE_EXECUTE_READWRITE and((unsigned int)mbi.AllocationBase%0x10000)==0)
            {
                potentional[j]=(unsigned int)mbi.AllocationBase;
                j++;
            }
            if(i<(unsigned int)mbi.BaseAddress+mbi.RegionSize-0x1000)
                i=(unsigned int)mbi.BaseAddress+mbi.RegionSize-0x1000;
        }
        i+=0x1000;
    }
    char tmp[10]="";
    for(int i=0; i<j; i++)
    {
        if(IsCodeSplicingPage(hProcess, potentional[i]))
        {
            sprintf(tmp, "%X", potentional[i]);
            SetDlgItemTextA(hwndDlg, IDC_EDT_ADDR, tmp);
            if(VirtualQueryEx(hProcess, (void*)potentional[i], &mbi, sizeof(MEMORY_BASIC_INFORMATION)))
                sprintf(tmp, "%X", (unsigned int)mbi.RegionSize);
            else
                strcpy(tmp, "20000");
            SetDlgItemTextA(hwndDlg, IDC_EDT_SIZE, tmp);
            CloseHandle(hProcess);
            return true;
        }
    }
    unsigned bb=GetTickCount()-aa;
    printf("%d, %X\n", bb, bb);
    CloseHandle(hProcess);
    return false;
}

//Get the total number of spliced blocks in a page
unsigned int GetSpliceCount(SPLICE_INFO* si)
{
    DISASM MyDisasm= {0};
    unsigned int len=0, i=0, j=0;
    int Error=0;
    MyDisasm.EIP=(UIntPtr)si->data;
    while(!Error and i<si->size)
    {
        len=Disasm(&MyDisasm);
        if(len!=(unsigned int)UNKNOWN_OPCODE)
        {
            if(MyDisasm.Instruction.BranchType==JmpType and MyDisasm.Instruction.Opcode==0xE9)
            {
                unsigned int* jump_size=(unsigned int*)(unsigned int)(MyDisasm.EIP+1);
                if(*jump_size)
                    j++;
            }
            MyDisasm.EIP+=len;
            i+=len;
        }
        else
            Error=1;
    }
    return j;
}

//Fill our structure with information
bool FillSpliceStruct(SPLICE_INFO* si)
{
    DISASM MyDisasm= {0};
    unsigned int len=0, i=0, j=0;
    int Error=0;
    MyDisasm.VirtualAddr=si->va;
    MyDisasm.EIP=(UIntPtr)si->data;
    unsigned char* begindata=si->data;
    unsigned int beginva=si->va;
    while(!Error and i<si->size)
    {
        len=Disasm(&MyDisasm);
        if(len!=(unsigned int)UNKNOWN_OPCODE)
        {
            if(MyDisasm.Instruction.BranchType==JmpType and MyDisasm.Instruction.Opcode==0xE9)
            {
                unsigned int* jump_size=(unsigned int*)(unsigned int)(MyDisasm.EIP+1);
                if(*jump_size) //Prevents E9 00000000 construction
                {
                    si->splices[j].data=begindata;
                    begindata=(unsigned char*)MyDisasm.EIP+len;
                    si->splices[j].va=beginva;
                    beginva=MyDisasm.VirtualAddr+len;
                    si->splices[j].size=beginva-si->splices[j].va;
                    si->splices[j].jmp_va=(*jump_size)+MyDisasm.VirtualAddr;
                    j++;
                }
            }
            MyDisasm.EIP+=len;
            MyDisasm.VirtualAddr+=len;
            i+=len;
        }
        else
            Error=1;
    }
    return 1-Error;
}

//Fill the basic structure
bool GetSpliceInfo(HWND hwndDlg, unsigned int addr, unsigned int size, HANDLE hProcess)
{
    SPLICE_INFO* si=splice_info;
    if(si)
    {
        if(si->data)
            free(si->data);
        if(si->splices)
            free(si->splices);
        free(si);
    }
    splice_info=(SPLICE_INFO*)malloc(sizeof(SPLICE_INFO));
    si=splice_info;
    memset(si, 0, sizeof(SPLICE_INFO));
    si->va=addr;
    si->size=size;
    si->data=(unsigned char*)malloc(si->size);
    if(!rpm(hProcess, si->va, si->data, si->size))
        return false;
    si->total_splices=GetSpliceCount(si);
    char tmp[10]="";
    sprintf(tmp, "%d", si->total_splices);
    SetDlgItemTextA(hwndDlg, IDC_EDT_NUMSPLICES, tmp);
    si->splices=(SPLICE_ENTRY*)malloc(sizeof(SPLICE_ENTRY)*si->total_splices);
    if(!FillSpliceStruct(si))
        return false;
    return true;
}

//Rebase a page
bool RebaseSplices(unsigned int base, HANDLE hProcess)
{
    puts("rebaseSplices");
    SPLICE_INFO* si=splice_info;
    if(!wpm(hProcess, base, si->data, si->size))
        return false;
    int diff=si->va-base;
    for(unsigned int i=0; i<si->total_splices; i++)
    {
        si->splices[i].va-=diff;
        int* jump_dw=(int*)(((int)(si->splices[i].data))+si->splices[i].size-4);
        *jump_dw+=diff;
        int new_dw=0;
        if(!rpm(hProcess, si->splices[i].jmp_va+1, &new_dw, 4))
            return false;
        new_dw-=diff;
        if(!wpm(hProcess, si->splices[i].jmp_va+1, &new_dw, 4))
            return false;
    }
    if(!wpm(hProcess, base, si->data, si->size))
        return false;
    unsigned char* zero=(unsigned char*)malloc(si->size);
    memset(zero, 0, si->size);
    if(!wpm(hProcess, si->va, zero, si->size))
        return false;
    free(zero);
    return true;
}

void SortByteArray(char* a, int size)
{
    char* cpy=(char*)malloc(size*sizeof(char));
    memcpy(cpy, a, size*4);
    char* biggest=&cpy[0];
    for(int i=0; i<size; i++)
    {
        for(int j=0; j<size; j++)
        {
            if(cpy[j]>*biggest)
                biggest=&cpy[j];
        }
        a[size-i-1]=*biggest;
        *biggest=0;
    }
}

void FormatArgCmp(char* text)
{
    int len=strlen(text);
    _strlwr(text);
    char* tmp=(char*)malloc(len+1);
    memset(tmp, 0, len+1);
    for(int i=0,j=0; i<len; i++)
        if(isalnum(text[i]))
            j+=sprintf(tmp+j, "%c", text[i]);
    strcpy(text, tmp);
    free(tmp);
}

bool CompareArguments(DISASM* a, DISASM* b)
{
    const char* c=0;
    const char* d=0;
    c=a->Argument1.ArgMnemonic;
    if(!*c)
    {
        c=a->Argument2.ArgMnemonic;
        if(!*c)
        {
            c=a->Argument3.ArgMnemonic;
            if(!*c)
                c=0;
        }
    }
    d=b->Argument1.ArgMnemonic;
    if(!*d)
    {
        d=b->Argument2.ArgMnemonic;
        if(!*d)
        {
            d=b->Argument3.ArgMnemonic;
            if(!*d)
                d=0;
        }
    }
    if(!c or !d)
        return false;
    if(!strcasecmp(c, d))
        return true;
    return false;
}

bool CompareArgumentsNoOrder(DISASM* a, DISASM* b)
{
    if(((((a->Argument1.ArgType^a->Argument2.ArgType)&0xFFFF)^((b->Argument1.ArgType^b->Argument2.ArgType)&0xFFFF)))==0)
        return true;
    return false;

    /*char final_buffer1[16*3]="";
    char final_buffer2[16*3]="";
    const char* arg=0;
    arg=a->Argument1.ArgMnemonic;
    if(*arg)
        strcat(final_buffer1, arg);
    arg=a->Argument2.ArgMnemonic;
    if(*arg)
        strcat(final_buffer1, arg);
    arg=a->Argument3.ArgMnemonic;
    if(*arg)
        strcat(final_buffer1, arg);
    FormatArgCmp(final_buffer1);
    SortByteArray(final_buffer1, strlen(final_buffer1));
    arg=b->Argument1.ArgMnemonic;
    if(*arg)
        strcat(final_buffer2, arg);
    arg=b->Argument2.ArgMnemonic;
    if(*arg)
        strcat(final_buffer2, arg);
    arg=b->Argument3.ArgMnemonic;
    if(*arg)
        strcat(final_buffer2, arg);
    FormatArgCmp(final_buffer2);
    SortByteArray(final_buffer2, strlen(final_buffer2));
    if(!strcmp(final_buffer1, final_buffer2))
        return true;
    return false;*/
}

bool CompareArgumentsSame(DISASM* a)
{
    if(a->Argument1.ArgType!=a->Argument2.ArgType)
        return false;
    const char* b=0;
    const char* c=0;
    b=a->Argument1.ArgMnemonic;
    c=a->Argument2.ArgMnemonic;
    char tmp1[16]="";
    strcpy(tmp1, b);
    FormatArgCmp(tmp1);
    char tmp2[16]="";
    strcpy(tmp2, c);
    FormatArgCmp(tmp2);
    if(!strcmp(tmp1, tmp2))
        return true;
    return false;
}

bool RemoveJumps(unsigned char* data, unsigned int size)
{
    DISASM* MyDisasm=(DISASM*)malloc(sizeof(DISASM));
    memset(MyDisasm, 0, sizeof(DISASM));
    unsigned int len=0, i=0;
    int Error=0;
    MyDisasm->EIP=(UIntPtr)data;
    while(!Error and i<size)
    {
        len=Disasm(MyDisasm);
        if(len!=(unsigned int)UNKNOWN_OPCODE)
        {
            if(MyDisasm->Instruction.BranchType)
                memset((void*)MyDisasm->EIP, 0x90, len);
            MyDisasm->EIP+=len;
            i+=len;
        }
        else
            Error=1;
    }
    free(MyDisasm);
    return 1-Error;
}

/*bool RemovePushPop(unsigned char* data, unsigned int size, DISASM* MyDisasm_old, unsigned int len_old)
{
    DISASM* MyDisasm=(DISASM*)malloc(sizeof(DISASM));
    memset(MyDisasm, 0, sizeof(DISASM));
    unsigned int len=0, i=0;
    int Error=0;
    MyDisasm->EIP=(UIntPtr)data;
    while(!Error and i<size)
    {
        len=Disasm(MyDisasm);
        if(len!=(unsigned int)UNKNOWN_OPCODE)
        {
            if(!strncasecmp(MyDisasm->Instruction.Mnemonic, "push", 4))
            {
                if(!RemovePushPop((unsigned char*)MyDisasm->EIP+len, size-i-len, MyDisasm, len))
                {
                    free(MyDisasm);
                    return false;
                }
            }
            else if(len_old and !strncasecmp(MyDisasm->Instruction.Mnemonic, "pop", 3))
            {
                bool ret=false;
                if(CompareArguments(MyDisasm, MyDisasm_old))
                {
                    memset((void*)MyDisasm_old->EIP, 0x90, len_old);
                    memset((void*)MyDisasm->EIP, 0x90, len);
                    ret=true;
                }
                free(MyDisasm);
                return ret;
            }
            MyDisasm->EIP+=len;
            i+=len;
        }
        else
            Error=1;
    }
    free(MyDisasm);
    return false;
}*/

bool RemoveBswap(unsigned char* data, unsigned int size, DISASM* MyDisasm_old, unsigned int len_old)
{
    DISASM* MyDisasm=(DISASM*)malloc(sizeof(DISASM));
    memset(MyDisasm, 0, sizeof(DISASM));
    unsigned int len=0, i=0;
    int Error=0;
    MyDisasm->EIP=(UIntPtr)data;
    while(!Error and i<size)
    {
        len=Disasm(MyDisasm);
        if(len!=(unsigned int)UNKNOWN_OPCODE)
        {
            if(!strncasecmp(MyDisasm->Instruction.Mnemonic, "bswap", 5))
            {
                if(len_old and CompareArguments(MyDisasm, MyDisasm_old))
                {
                    memset((void*)MyDisasm_old->EIP, 0x90, len_old);
                    memset((void*)MyDisasm->EIP, 0x90, len);
                    free(MyDisasm);
                    return true;
                }
                RemoveBswap((unsigned char*)MyDisasm->EIP+len, size-i-len, MyDisasm, len);
            }
            MyDisasm->EIP+=len;
            i+=len;
        }
        else
            Error=1;
    }
    free(MyDisasm);
    return 1-Error;
}

bool IsInverse(const char* a, const char* b)
{
    if(!strncasecmp(a, "push", 4) and !strncasecmp(b, "pop", 3))
        return true;
    else if(!strncasecmp(b, "push", 4) and !strncasecmp(a, "pop", 3))
        return true;
    return false;
}

bool ModifiesRegister(DISASM* a, DISASM* b)
{
    int len=strlen(a->Instruction.Mnemonic);
    if(!strncasecmp(a->CompleteInstr+len, b->Argument1.ArgMnemonic, len))
        return true;
    return false;
}

bool RemovePushPop(unsigned char* data, unsigned int size, DISASM* MyDisasm_old, unsigned int len_old, int stack_balance)
{
    DISASM* MyDisasm=(DISASM*)malloc(sizeof(DISASM));
    memset(MyDisasm, 0, sizeof(DISASM));
    unsigned int len=0, i=0;
    int Error=0;
    MyDisasm->EIP=(UIntPtr)data;
    while(!Error and i<size)
    {
        len=Disasm(MyDisasm);
        if(len!=(unsigned int)UNKNOWN_OPCODE)
        {
            if(len_old)
            {
                if(IsInverse(MyDisasm->Instruction.Mnemonic, MyDisasm_old->Instruction.Mnemonic))
                {
                    if(CompareArguments(MyDisasm, MyDisasm_old))
                    {
                        memset((void*)MyDisasm_old->EIP, 0x90, len_old);
                        memset((void*)MyDisasm->EIP, 0x90, len);
                        free(MyDisasm);
                        return true;
                    }
                }
                else if(ModifiesRegister(MyDisasm_old, MyDisasm) and strcasecmp(MyDisasm->Instruction.Mnemonic, MyDisasm_old->Instruction.Mnemonic))
                    return false;
            }
            if(!strncasecmp(MyDisasm->Instruction.Mnemonic, "push", 4))
            {
                if(RemovePushPop((unsigned char*)MyDisasm->EIP+len, size-i-len, MyDisasm, len, stack_balance+1))
                {
                    free(MyDisasm);
                    return true;
                }
            }
            MyDisasm->EIP+=len;
            i+=len;
        }
        else
            Error=1;
    }
    free(MyDisasm);
    return 1-Error;
}

bool RemoveNot(unsigned char* data, unsigned int size, DISASM* MyDisasm_old, unsigned int len_old)
{
    DISASM* MyDisasm=(DISASM*)malloc(sizeof(DISASM));
    memset(MyDisasm, 0, sizeof(DISASM));
    unsigned int len=0, i=0;
    int Error=0;
    MyDisasm->EIP=(UIntPtr)data;
    while(!Error and i<size)
    {
        len=Disasm(MyDisasm);
        if(len!=(unsigned int)UNKNOWN_OPCODE)
        {
            if(!strncasecmp(MyDisasm->Instruction.Mnemonic, "not", 3))
            {
                if(len_old and CompareArguments(MyDisasm, MyDisasm_old))
                {
                    memset((void*)MyDisasm_old->EIP, 0x90, len_old);
                    memset((void*)MyDisasm->EIP, 0x90, len);
                    free(MyDisasm);
                    return true;
                }
                RemoveNot((unsigned char*)MyDisasm->EIP+len, size-i-len, MyDisasm, len);
            }
            MyDisasm->EIP+=len;
            i+=len;
        }
        else
            Error=1;
    }
    free(MyDisasm);
    return 1-Error;
}

bool RemoveXchg(unsigned char* data, unsigned int size, DISASM* MyDisasm_old, unsigned int len_old)
{
    DISASM* MyDisasm=(DISASM*)malloc(sizeof(DISASM));
    memset(MyDisasm, 0, sizeof(DISASM));
    unsigned int len=0, i=0;
    int Error=0;
    MyDisasm->EIP=(UIntPtr)data;
    while(!Error and i<size)
    {
        len=Disasm(MyDisasm);
        if(len!=(unsigned int)UNKNOWN_OPCODE)
        {
            if(!strncasecmp(MyDisasm->Instruction.Mnemonic, "xchg", 4))
            {
                if(len_old and CompareArgumentsNoOrder(MyDisasm, MyDisasm_old))
                {
                    memset((void*)MyDisasm_old->EIP, 0x90, len_old);
                    memset((void*)MyDisasm->EIP, 0x90, len);
                    free(MyDisasm);
                    return true;
                }
                RemoveXchg((unsigned char*)MyDisasm->EIP+len, size-i-len, MyDisasm, len);
            }
            MyDisasm->EIP+=len;
            i+=len;
        }
        else
            Error=1;
    }
    free(MyDisasm);
    return 1-Error;
}

bool RemoveNopAlike(unsigned char* data, unsigned int size)
{
    DISASM* MyDisasm=(DISASM*)malloc(sizeof(DISASM));
    memset(MyDisasm, 0, sizeof(DISASM));
    unsigned int len=0, i=0;
    int Error=0;
    MyDisasm->EIP=(UIntPtr)data;
    while(!Error and i<size)
    {
        len=Disasm(MyDisasm);
        if(len!=(unsigned int)UNKNOWN_OPCODE)
        {
            if(!strncasecmp(MyDisasm->Instruction.Mnemonic, "xchg", 4))
            {
                if(CompareArgumentsSame(MyDisasm))
                    memset((void*)MyDisasm->EIP, 0x90, len);
            }
            else if(!strncasecmp(MyDisasm->Instruction.Mnemonic, "mov", 3))
            {
                if(CompareArgumentsSame(MyDisasm))
                {
                    if(strncasecmp(MyDisasm->Argument1.ArgMnemonic, "edi", 3))
                        memset((void*)MyDisasm->EIP, 0x90, len);
                    else
                    {
                        //TODO: mov edi,edi
                    }
                }
            }
            MyDisasm->EIP+=len;
            i+=len;
        }
        else
            Error=1;
    }
    free(MyDisasm);
    return 1-Error;
}

bool SortNops(unsigned char* data, unsigned int size)
{
    DISASM* MyDisasm=(DISASM*)malloc(sizeof(DISASM));
    memset(MyDisasm, 0, sizeof(DISASM));
    unsigned int len=0, i=0, j=0;
    int Error=0;
    MyDisasm->EIP=(UIntPtr)data;
    unsigned char* sorted_data=(unsigned char*)malloc(size);
    memset(sorted_data, 0x90, size);
    while(!Error and i<size)
    {
        while(*((unsigned char*)MyDisasm->EIP)==0x90)
        {
            i++;
            MyDisasm->EIP++;
        }
        if(i>=size)
            break;
        len=Disasm(MyDisasm);
        if(len!=(unsigned int)UNKNOWN_OPCODE)
        {
            memcpy(sorted_data+j, (void*)MyDisasm->EIP, len);
            j+=len;
            MyDisasm->EIP+=len;
            i+=len;
        }
        else
            Error=1;
    }
    free(MyDisasm);
    free(sorted_data);
    return 1-Error;
}

void RemoveSplices(unsigned char* data, unsigned int size)
{
    RemoveJumps(data, size);
    RemoveNopAlike(data, size);
    RemoveBswap(data, size, 0, 0);
    RemoveNot(data, size, 0, 0);
    RemoveXchg(data, size, 0, 0);
    RemovePushPop(data, size, 0, 0, true);
    SortNops(data, size);
}

bool ProcessSplices(HWND list, HANDLE hProcess)
{
    SPLICE_INFO* si=splice_info;
    for(unsigned int i=0; i<si->total_splices; i++)
    {
        RemoveSplices(si->splices[i].data, si->splices[i].size-5);
        printf("%X\n", si->splices[i].data-si->data+si->va);
    }
    if(!wpm(hProcess, si->va, si->data, si->size))
        return false;
    return true;
}

//Obtain debug privilege (by CondZero)
int LoadSeDebugPrivilege(void)
{
    HANDLE hToken=0;
    LUID Val;
    TOKEN_PRIVILEGES tp;

    if (!OpenProcessToken(GetCurrentProcess(),TOKEN_ADJUST_PRIVILEGES
                          | TOKEN_QUERY, &hToken))
        return(GetLastError());

    if (!LookupPrivilegeValue(NULL, SE_DEBUG_NAME, &Val))
        return(GetLastError());

    tp.PrivilegeCount = 1;
    tp.Privileges[0].Luid = Val;
    tp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;

    if (!AdjustTokenPrivileges(hToken, FALSE, &tp,
                               sizeof (tp), NULL, NULL))
        return(GetLastError());

    CloseHandle(hToken);

    return 1;
}

//Main dialog callback
BOOL CALLBACK DlgMain(HWND hwndDlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    switch(uMsg)
    {
    case WM_INITDIALOG:
    {
        if(LoadSeDebugPrivilege()!=1)
            MessageBoxA(hwndDlg, "Could not obtain debug privilege...", "Problem?", MB_ICONERROR);
        SendMessageA(hwndDlg, WM_SETICON, ICON_BIG, (LPARAM)LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICON)));
        GetProcessList(GetDlgItem(hwndDlg, IDC_COMBO_PROCESSLIST));
    }
    return TRUE;

    case WM_CLOSE:
    {
        EndDialog(hwndDlg, 0);
    }
    return TRUE;

    case WM_COMMAND:
    {
        switch(LOWORD(wParam))
        {
        case IDC_BTN_REMOVE:
        {
            SetFocus(0);
            HANDLE hProcess=OpenProcess(PROCESS_ALL_ACCESS, 0, current_pid);
            if(!hProcess)
            {
                PrintError(hwndDlg, "OpenProcess failed...");
                return TRUE;
            }
            ProcessSplices(GetDlgItem(hwndDlg, IDC_LIST_SPLICES), hProcess);
        }
        return TRUE;

        case IDC_BTN_REBASE:
        {
            SetFocus(0);
            unsigned int base=0;
            char tmp[10]="";
            GetDlgItemTextA(hwndDlg, IDC_EDT_BASE, tmp, 10);
            sscanf(tmp, "%X", &base);
            if(!base)
            {
                PrintError(hwndDlg, "Incorrect base!");
                return TRUE;
            }
            HANDLE hProcess=OpenProcess(PROCESS_ALL_ACCESS, 0, current_pid);
            if(!hProcess)
            {
                PrintError(hwndDlg, "OpenProcess failed...");
                return TRUE;
            }
            if(!RebaseSplices(base, hProcess))
            {
                PrintError(hwndDlg, "WriteMemory error!");
                return TRUE;
            }
            SetDlgItemTextA(hwndDlg, IDC_EDT_ADDR, tmp);
            SetDlgItemTextA(hwndDlg, IDC_STC_LOG, "CodeSplices rebased!");
            MessageBeep(MB_ICONINFORMATION);
        }
        return TRUE;

        case IDC_BTN_ANALYZE:
        {
            SetFocus(0);
            unsigned int addr=0;
            unsigned int size=0;
            char tmp[10]="";
            GetDlgItemTextA(hwndDlg, IDC_EDT_ADDR, tmp, 10);
            sscanf(tmp, "%X", &addr);
            GetDlgItemTextA(hwndDlg, IDC_EDT_SIZE, tmp, 10);
            sscanf(tmp, "%X", &size);
            HANDLE hProcess=OpenProcess(PROCESS_ALL_ACCESS, 0, current_pid);
            if(!addr or !size or !hProcess)
            {
                PrintError(hwndDlg, "Invalid data entered!");
                return TRUE;
            }
            bool good=GetSpliceInfo(hwndDlg, addr, size, hProcess);
            EnableWindow(GetDlgItem(hwndDlg, IDC_BTN_REBASE), good);
            EnableWindow(GetDlgItem(hwndDlg, IDC_BTN_REMOVE), good);
        }
        return TRUE;

        case IDC_COMBO_PROCESSLIST:
        {
            switch(HIWORD(wParam))
            {
            case CBN_DROPDOWN:
            {
                SetFocus(0);
                GetProcessList(GetDlgItem(hwndDlg, IDC_COMBO_PROCESSLIST));
            }
            return TRUE;

            case CBN_SELCHANGE:
            {
                SetFocus(0);
                current_pid=processlist[process_count-SendDlgItemMessageA(hwndDlg, LOWORD(wParam), CB_GETCURSEL, 0, 0)-1].pid;
                SetDlgItemTextA(hwndDlg, IDC_EDT_ADDR, "");
                SetDlgItemTextA(hwndDlg, IDC_EDT_SIZE, "");
                EnableWindow(GetDlgItem(hwndDlg, IDC_BTN_REBASE), 0);
                EnableWindow(GetDlgItem(hwndDlg, IDC_BTN_REMOVE), 0);
                SetDlgItemTextA(hwndDlg, IDC_STC_LOG, "");
                CheckSplices(hwndDlg);
            }
            return TRUE;
            }
        }
        return TRUE;

        case IDC_CHK_ONTOP:
        {
            SetFocus(0);
            if(IsDlgButtonChecked(hwndDlg, LOWORD(wParam)))
                SetWindowPos(hwndDlg, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE|SWP_NOSIZE|SWP_SHOWWINDOW);
            else
                SetWindowPos(hwndDlg, HWND_NOTOPMOST, 0, 0, 0, 0, SWP_NOMOVE|SWP_NOSIZE|SWP_SHOWWINDOW);
        }
        return TRUE;
        }
    }
    return TRUE;
    }
    return FALSE;
}

int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd)
{
    unsigned char table[] =
    {
        0x57, 0x52, 0xBA, 0x01, 0x00, 0x00, 0x00, 0x5A, 0x5F
    };
    RemoveSplices(table, sizeof(table));
    hInst=hInstance;
    InitCommonControls();
    //dummy();
    return DialogBox(hInst, MAKEINTRESOURCE(DLG_MAIN), NULL, (DLGPROC)DlgMain);
}
